function caricadati() {
  var num1 = 0;
  var num2 = 0;
  var operatore = "+";
  var risultato = 0;

  //memorizzo in un vettore tutti i miei riferimenti agli elementi "input"
  vettore = document.getElementsByTagName("input");

  //memorizzo il riferimento al tag con id=info (il mio paragrafo)
  informazioni = document.getElementById("info");
}

function cifra(interfaccia, num) {
    //per concatenare i numeri
  interfaccia.schermo.value += num;
}

function operazione(interfaccia, op) {
  //do delle informazioni
  informazioni.textContent = "Inserisci il secondo numero per la tua " + op;
  //memorizzo l'operatore
  operatore = op;
  //disabilito tutti i tasti operazione
  interfaccia.piu.disabled = true;
  interfaccia.meno.disabled = true;
  interfaccia.per.disabled = true;
  interfaccia.diviso.disabled = true;
  //abilito l'uguale ed il punto
  interfaccia.uguale.disabled = false;
  interfaccia.punto.disabled = false;
  //memorizzo il primo numero o 0 se è vuoto
  num1 = interfaccia.schermo.value;
  //cancello lo schermo
  interfaccia.schermo.value = " ";
}

function esegui(interfaccia, num) {
  //do le informazioni
  informazioni.textContent = "Premi il bottone C per ricominciare";
  //memorizzo il primo numero o 0 se è vuoto
  num2 = interfaccia.schermo.value;
  //controllo i numeri
  if (num1 == " ") {
    num1 = "0";
  }
  if (num2 == " ") {
    num2 = "0";
  }
  //calcolo il risultato
  risultato = eval(num1 + operatore + num2);
  //visualizzo il risultato
  interfaccia.schermo.value = risultato;
  //disabilito tutti
  for (var i = 0; i < vettore.length; i++) {
    vettore[i].disabled = true;
  }
  //abilito il tasto cancella per il ripristino
  interfaccia.cancella.disabled = false;
}

function dacapo(interfaccia) {
  //do le informazioni
  informazioni.textContent = "Inserisci il primo numero";

  num1 = 0;
  num2 = 0;
  operatore = "+";
  risultato = 0;

  //abilito tutto
  for (var i = 0; i < vettore.length; i++) {
    vettore[i].disabled = false;
  }

  //disabilito l'uguale
  interfaccia.uguale.disabled = true;

  //cancello lo schermo
  interfaccia.schermo.value = " ";
}
